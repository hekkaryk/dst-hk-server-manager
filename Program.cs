﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

namespace dst_hk_server_manager
{
    class Program
    {
        const string sDSTUpdate = "update";
        const string sMaster = "Master";
        const string sCaves = "Caves";
        const string fileNameSettings = "settings.json";
        const string fileNameStrings = "strings.json";
        static string fullNameDST_DS;
        static string fullNameMaster;
        static string fullNameCaves;

        static Settings settings = new Settings();
        static Strings strings = new Strings();

        static DateTime lastUpdated = DateTime.MinValue;
        static bool valid = true;
        static bool shouldUpdate = true;
        static DateTime lastHeartbeat = DateTime.UtcNow;

        static Dictionary<string, ProcessStartInfo> psi = new Dictionary<string, ProcessStartInfo>();
        static Dictionary<string, string> processes = new Dictionary<string, string>()
        {
            {sDSTUpdate, null }
        };
        static Dictionary<string, bool> running = new Dictionary<string, bool>()
        {
            { sDSTUpdate, false },
            { sMaster, false },
            { sCaves, false }
        };

        static bool IsProcessRunning(string name)
        {
            if (name == null) return false;
            Process[] proc = Process.GetProcessesByName(name);
            return proc.Length > 0 ? true : false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName">Name of DST_DS dedicated executable</param>
        /// <param name="cmdTitle">Title to display in console window</param>
        /// <param name="batchName">Name of batch file to create</param>
        /// <param name="shardName">Name of shard to start</param>
        static void CreateSeparateEXE(string fileName)
        {
            try
            {
                string path = Path.Combine(settings.PathDST_DS, fileName);
                if (settings.Mode == "unsafe" && File.Exists(path))
                {
                    File.Delete(path);
                }
                if (!File.Exists(path))
                {
                    Console.WriteLine($"{DateTime.Now}: {strings.CreatingEXE}");
                    File.Copy(fullNameDST_DS, path);
                }
            }
            catch (Exception exception) when (exception is FileNotFoundException || exception is DirectoryNotFoundException)
            {
                Console.WriteLine($"{DateTime.Now}: {strings.DST_DS_NotFound}");
                valid = false;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{DateTime.Now}: {strings.CouldNotCreate} {fileName}{Environment.NewLine}{e.Message}");
            }
        }

        static void StartProcess(string name)
        {
            Console.WriteLine($"{DateTime.Now}: {strings.Starting} {name}");
            try
            {
                processes[name] = Process.Start(psi[name]).ProcessName;
            }
            catch (Exception exception) when (exception is FileNotFoundException || exception is DirectoryNotFoundException)
            {
                Console.WriteLine($"{DateTime.Now}: {name} {strings.NotFound}");
                valid = false;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{DateTime.Now}: {strings.CouldNotStart} {name}{Environment.NewLine}{e.Message}");
            }
        }

        static T ReadJSON<T>(T original, string fileName)
        {
            try
            {
                if (File.Exists(fileName))
                {
                    using (StreamReader reader = new StreamReader(fileName))
                    {
                        string json = reader.ReadToEnd();
                        return JsonConvert.DeserializeObject<T>(json);
                    }
                }
            }
            catch (Exception exception) when (exception is FileNotFoundException || exception is DirectoryNotFoundException)
            {
                Console.WriteLine($"{DateTime.Now}: {fileName} {strings.NotFound}");
            }
            catch (Exception)
            {
                Console.WriteLine($"{DateTime.Now}: {fileName} {strings.InvalidJSON}");
            }
            return original;
        }

        static void InitializeLocals()
        {
            fullNameDST_DS = Path.Combine(settings.PathDST_DS, settings.FileNameDST_DS);
            fullNameMaster = Path.Combine(settings.PathDST_DS, settings.EXEMaster);
            fullNameCaves = Path.Combine(settings.PathDST_DS, settings.EXECaves);
            processes.Add(settings.EXEMaster, null);
            processes.Add(settings.EXECaves, null);
            psi[sDSTUpdate] = new ProcessStartInfo()
            {
                CreateNoWindow = false,
                WorkingDirectory = settings.PathSteamCMD,
                FileName = settings.FileNameSteamCMD,
                Arguments = "+login anonymous +app_update 343050 validate +quit"
            };
            psi.Add(settings.EXEMaster, new ProcessStartInfo()
            {
                CreateNoWindow = false,
                WorkingDirectory = settings.PathDST_DS,
                FileName = settings.EXEMaster,
                Arguments = $"-console -cluster {settings.Cluster} -shard {sMaster}"
            });
            psi.Add(settings.EXECaves, new ProcessStartInfo()
            {
                CreateNoWindow = false,
                WorkingDirectory = settings.PathDST_DS,
                FileName = settings.EXECaves,
                Arguments = $"-console -cluster {settings.Cluster} -shard {sCaves}"
            });
        }

        static void Main(string[] args)
        {
            settings = ReadJSON(settings, fileNameSettings);
            strings = ReadJSON(strings, fileNameStrings);
            InitializeLocals();
            while (valid)
            {
                if (shouldUpdate && !running[sDSTUpdate] && !running[sMaster] && !running[sCaves])
                {
                    StartProcess(sDSTUpdate);
                    lastUpdated = DateTime.UtcNow;
                    shouldUpdate = false;
                }
                running[sDSTUpdate] = IsProcessRunning(processes[sDSTUpdate]);

                if (!running[sDSTUpdate] && !running[sMaster])
                {
                    CreateSeparateEXE(settings.EXEMaster);
                    StartProcess(settings.EXEMaster);
                }
                running[sMaster] = IsProcessRunning(processes[settings.EXEMaster]);

                if (!running[sDSTUpdate] && !running[sCaves])
                {
                    CreateSeparateEXE(settings.EXECaves);
                    StartProcess(settings.EXECaves);
                }
                running[sCaves] = IsProcessRunning(processes[settings.EXECaves]);

                if (lastUpdated.AddMinutes(settings.DelayMinUpdate) < DateTime.UtcNow)
                {
                    shouldUpdate = true;
                }

                if (lastHeartbeat.AddMinutes(settings.DelayMinHeartbeat) < DateTime.UtcNow)
                {
                    Console.WriteLine($"{DateTime.Now}: {strings.Heartbeat}");
                    lastHeartbeat = DateTime.UtcNow;
                }

                if (valid)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(settings.DelaySecCheck));
                }
            }
            Console.WriteLine($"{strings.InvalidConfiguration}");
            Console.WriteLine(string.Format("{1}{0}{1}{2}{1}{1}{3}{1}{4}",
                strings.PleaseFixOrAddConfig,
                Environment.NewLine,
                JsonConvert.SerializeObject(settings, Formatting.Indented),
                strings.MaybeTranslate,
                JsonConvert.SerializeObject(strings,Formatting.Indented)));
            Console.WriteLine(strings.PressEnterToExit);
            Console.ReadLine();
        }
    }
}
