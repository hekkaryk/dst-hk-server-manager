﻿namespace dst_hk_server_manager
{
    class Strings
    {
        public string CreatingEXE { get; set; } = "Creating dedicated EXE for management purposes";
        public string DST_DS_NotFound { get; set; } = "Invalid configuration - could not find DST:DS executables. Please update settings.json";
        public string CouldNotCreate { get; set; } = "Could not create";
        public string Starting { get; set; } = "Starting";
        public string NotFound { get; set; } = "not found";
        public string CouldNotStart { get; set; } = "Could not start";
        public string InvalidJSON { get; set; } = "JSON syntax invalid";
        public string Heartbeat { get; set; } = "Heartbeat";
        public string InvalidConfiguration { get; set; } = "Invalid configuration (or removal of required executables)";
        public string PleaseFixOrAddConfig { get; set; } = "Please create/update settings.json containing following in this app directory:";
        public string PressEnterToExit { get; set; } = "Press enter to exit";
        public string MaybeTranslate { get; set; } = "If you want you can create a translation of this tool by creating strings.json containing following in this app directory:";
    }
}
