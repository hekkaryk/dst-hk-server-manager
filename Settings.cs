﻿namespace dst_hk_server_manager
{
    class Settings
    {
        public string Mode { get; set; } = "safe";
        public string PathSteamCMD { get; set; } = @"C:\steamcmd\";
        public string PathDST_DS { get; set; } = @"C:\Program Files (x86)\Steam\steamapps\common\Don't Starve Together Dedicated Server\bin\";
        public string Cluster { get; set; } = "Cluster_1";
        public string EXEMaster { get; set; } = "master_worker.exe";
        public string EXECaves { get; set; } = "caves_worker.exe";
        public int DelaySecCheck { get; set; } = 5;
        public int DelayMinUpdate { get; set; } = 15;
        public int DelayMinHeartbeat { get; set; } = 10;
        public string FileNameDST_DS {get;set; } = "dontstarve_dedicated_server_nullrenderer.exe";
        public string FileNameSteamCMD {get;set;}= "steamcmd.exe";
    }
}
